import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  checkmenu:any=false;

  constructor() { }

  ngOnInit() {
  }
  duma(){
    if(this.checkmenu == false){
      this.checkmenu = true;
      $("#sidebar-wrapper").addClass("width250");
      $("#wrapper").removeClass("tenkhac");
      $("#codinhdiv").removeClass("permanent");
    }
    else {
      this.checkmenu = false;
      $("#sidebar-wrapper").removeClass("width250");
      $("#wrapper").addClass("tenkhac");
      $("#codinhdiv").addClass("permanent");
    }
  }
}
