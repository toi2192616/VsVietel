import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './default-layout-basic/footer/footer.component';
import { HeaderComponent } from './default-layout-basic/header/header.component';
import { NavbarComponent } from './default-layout-basic/navbar/navbar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [FooterComponent, HeaderComponent, NavbarComponent],
  exports: [FooterComponent, HeaderComponent, NavbarComponent]
})
export class PartialsModule { }
