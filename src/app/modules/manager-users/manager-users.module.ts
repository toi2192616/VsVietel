import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagerUsersRoutingModule } from './manager-users-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatInputModule,
  MatSelectModule,
  MatTabsModule,
  MatSlideToggleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatButtonModule,
  MatRippleModule,
  MatTooltipModule,
  MatListModule,
  MatIconModule,
  MatTableModule,
  MatCheckboxModule,
  MatCardModule,
  MatRadioModule,
  MatPaginatorModule,
  MatSliderModule,
  MatButtonToggleModule
} from '../../../../node_modules/@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ManagerUsersRoutingModule,
    MatDialogModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatRippleModule,
    MatTooltipModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    MatCheckboxModule,
    MatCardModule,
    MatRadioModule,
    MatPaginatorModule,
    MatSliderModule,
    MatButtonToggleModule
  ],
  declarations: [ListUsersComponent]
})
export class ManagerUsersModule { }
