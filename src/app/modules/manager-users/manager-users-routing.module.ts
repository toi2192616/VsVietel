import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListUsersComponent} from './list-users/list-users.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'pages'
    },
    children: [
      {
        path: 'list-users',
        component: ListUsersComponent,
        data: {
          title: 'Danh sách Users'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerUsersRoutingModule { }
