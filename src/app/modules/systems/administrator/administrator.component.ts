import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {

  constructor() { }

  public chartType: string = 'pie';

  public chartData: Array<any> = [100, 150];

  public chartLabels: Array<any> = ['Đỗ kì thi', 'Trượt kì thi'];

  public chartColors: Array<any> = [{
    hoverBorderColor: ['rgba(0, 0, 0, 0.1)', 'rgba(0, 0, 0, 0.1)'],
    hoverBorderWidth: 0,
    backgroundColor: ["#46BFBD", "#F7464A"],
    hoverBackgroundColor: ["#5AD3D1", "#FF5A5E"]
  }];

  public chartCircleOptions: any = {
    responsive: true,
    legend: {
      display: true,
      position: 'right',
      fullWidth: false,
      fontSize: 15,
      labels: {
        boxWidth: 20,
        boxHeight: 2
      }
    }
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  ngOnInit() {
  }

}
