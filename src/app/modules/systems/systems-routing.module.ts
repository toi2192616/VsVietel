import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministratorComponent } from './administrator/administrator.component';
import { CouseComponent } from './couse/couse.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'pages'
    },
    children: [
      {
        path: 'administrator',
        component: AdministratorComponent,
        data: {
          title: 'Trang cá nhân'
        }
      },
      {
        path: 'couse',
        component: CouseComponent,
        data: {
          title: 'Trang cá nhân'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemsRoutingModule { }
