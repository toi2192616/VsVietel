import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemsRoutingModule } from './systems-routing.module';
import { AdministratorComponent } from './administrator/administrator.component';
import { ChartsModule } from 'angular-bootstrap-md';
import { CouseComponent } from './couse/couse.component';


@NgModule({
  imports: [
    CommonModule,
    SystemsRoutingModule,
    ChartsModule
  ],
  declarations: [AdministratorComponent, CouseComponent]
})
export class SystemsModule { }
