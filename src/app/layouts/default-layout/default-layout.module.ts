import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '../../partials/partials.module';
import { DefaultLayoutRoutingModule } from './default-layout-routing.module';
import { DefaultAdminComponent } from './default-admin/default-admin.component';
import { ChartsModule } from 'angular-bootstrap-md';

@NgModule({
  imports: [
    CommonModule,
    DefaultLayoutRoutingModule,
    PartialsModule,
    ChartsModule
  ],
  declarations: [DefaultAdminComponent]
})
export class DefaultLayoutModule { }
