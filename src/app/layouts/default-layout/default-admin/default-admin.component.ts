import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-default-admin',
  templateUrl: './default-admin.component.html',
  styleUrls: ['./default-admin.component.css']
})
export class DefaultAdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }
}
