import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultAdminComponent } from './default-admin/default-admin.component';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'admin'
    },
    children: [
      {
        path: '',
        component: DefaultAdminComponent,
        data: {
          title: 'System'
        },
        children: [
          {
            path: 'systems',
            loadChildren: 'app/modules/systems/systems.module#SystemsModule',
          },
          {
            path: 'manager-users',
            loadChildren: 'app/modules/manager-users/manager-users.module#ManagerUsersModule',
          },
        ]
      },    
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DefaultLayoutRoutingModule { }
