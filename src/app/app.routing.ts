import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, HashLocationStrategy, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'default-template/systems/administrator',
    pathMatch: 'full',
  }, {
    path: 'default-template',
    
    data: {
      title: 'Default admin'
    },
    children: [
      {
        path: '',
        loadChildren: './layouts/default-layout/default-layout.module#DefaultLayoutModule'
      }
    ]
  },
];
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{useHash : true})
  ],
  exports: [
  ],
  providers: [
    
  ]
})
export class AppRoutingModule { 
 constructor(){
   console.log('vô route trước');
 }
}
